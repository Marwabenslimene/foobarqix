import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class FooBarQixTest {
	@Test
	void TestNumber1NotDivisible() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("1");
		String expected = "1";
		assertEquals(expected, result);
	}
	@Test
	void TestNumber2NotDivisible() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("2");
		String expected = "2";
		assertEquals(expected, result);
	}

	@Test
	void TestNumberIsDivisibleBy3() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("9");
		String expected = "Foo";
		assertEquals(expected, result);
	}

	@Test
	void TestNumberIsDivisibleBy5() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("10");
		String expected = "Bar*";
		assertEquals(expected, result);
	}
	
	@Test
	void TestNumberIsDivisibleBy7() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("14");
		String expected = "Qix";
		assertEquals(expected, result);
	}
	
	@Test
	void TestNumberContaines3DivisibleBy3() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("3");
		String expected = "FooFoo";
		assertEquals(expected, result);
	}
	@Test
	void TestNumberContaines5DivisibleBy5() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("5");
		String expected = "BarBar";
		assertEquals(expected, result);
	}
	@Test
	void TestNumberContaines555DivisibleBy5AndBy3() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("555");
		String expected = "FooBarBarBarBar";
		assertEquals(expected, result);
	}
	@Test
	void TestNumberContaines7DivisibleBy7() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("7");
		String expected = "QixQix";
		assertEquals(expected, result);
	}
	@Test
	void TestNumberContaines77DivisibleBy7() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("77");
		String expected = "QixQixQix";
		assertEquals(expected, result);
	}
	@Test
	void testNumber357DivisibleBy3And7() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("357");
		String expected = "FooQixFooBarQix";
		assertEquals(expected, result);
	}
	//101   => 1*1
	@Test
	void testNumberContains0NotDivisible() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("101");
		String expected = "1*1";
		assertEquals(expected, result);

	}
	//303   => FooFoo*Foo
	@Test
	void testNumber303Divisible() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("303");
		String expected = "FooFoo*Foo";
		assertEquals(expected, result);

	}
	//105   => FooBarQix*Bar
	@Test
	void testNumber105Divisible() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("105");
		String expected = "FooBarQix*Bar";
		assertEquals(expected, result);

	}
	//10101 => FooQix**
	@Test
	void testNumber10100Divisible() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("10101");
		String expected = "FooQix**";
		assertEquals(expected, result);

	}
	//53 => BarFoo
	@Test
	void TestNumber53() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("53");
		String expected = "BarFoo";
		assertEquals(expected, result);
	}
	//51 => FooBar
 
	@Test
	void TestNumber51() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("51");
		String expected = "FooBar";
		assertEquals(expected, result);
	}
	
	//13 => Foo
	
	@Test
	void TestNumber13() {
		FooBarQix fooBarQix = new FooBarQix();
		String result = fooBarQix.compute("13");
		String expected = "Foo";
		assertEquals(expected, result);
	}
}
