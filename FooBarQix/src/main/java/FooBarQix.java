
public class FooBarQix {

	public String compute(String s) {

		String result = "";
		int value = Integer.parseInt(s);
		if (!(value % 3 == 0) && !(value % 5 == 0) && !(value % 7 == 0) && !(s.contains("3")) && !(s.contains("7"))&& !(s.contains("5"))) {
			result = s.replace('0', '*');
		} else {
			if (value % 3 == 0) {
				result += "Foo";
			}
			if (value % 5 == 0) {
				result += "Bar";
			}
			if (value % 7 == 0) {
				result += "Qix";
			}
			for (char ch : s.toCharArray()) {
				if (ch == '3') {
					result += "Foo";
				}
				if (ch == '5') {
					result += "Bar";
				}
				if (ch == '7') {
					result += "Qix";
				}
				if (ch == '0') {
					result += "*";
				}
			}
		}
		return result;
	}
}
